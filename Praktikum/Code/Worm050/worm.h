// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//

#ifndef _WORM_H
#define _WORM_H

// Dimensions and bounds
#define NAP_TIME            60      // Time in milliseconds to sleep between updates of display
#define ROWS_RESERVED       4       // Lines reserved for the message area
#define MIN_NUMBER_OF_ROWS  3       // The guaranteed number of rows available for the board
#define MIN_NUMBER_OF_COLS  70      // The guaranteed number of columns available for the board
#define WORM_LENGTH         20      // Maximal length of the worm
// Symbols to display
#define SYMBOL_FREE_CELL            ' '
#define SYMBOL_BARRIER              '#'
#define SYMBOL_WORM_INNER_ELEMENT   '0'

// Codes for the array of positions
#define UNUSED_POS_ELEM     -1      // Unused element in the worm arrays of positions

// Game state codes
typedef enum GameStates { 
    WORM_GAME_ONGOING, 
    WORM_OUT_OF_BOUNDS,             // Left screen
    WORM_CROSSING,                  // Worm crosses itself
    WORM_GAME_QUIT,                 // User likes to quit
} gameSt_t;
    // WORM_GAME_ONGOING  = 0           //Game still ongoing 
    // WORM_OUT_OF_BOUNDS = 1           //Left screen  
    // WORM_GAME_QUIT     = 2           //User likes to quit

// Numbers for color pairs used by curses macro COLOR_PAIR
typedef enum ColorPairs { 
    COLP_USER_WORM = 1, 
    COLP_FREE_CELL = 2,
    COLP_BARRIER   = 3,
} colorP_t;

// Result codes of functions
typedef enum ResCodes { 
    RES_OK, 
    RES_FAILED, 
    RES_INTERNAL_ERROR,
} res_t;

#endif  // #define _WORM_H
