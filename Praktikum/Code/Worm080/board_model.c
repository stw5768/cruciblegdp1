// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The board model
#include <curses.h>
#include <stdlib.h>
#include "worm.h"
#include "board_model.h"
#include "messages.h"


// Place an item onto the curses display.
void placeItem(struct board* aboard, int y, int x, enum BoardCodes board_code,
               chtype symbol, enum ColorPairs color_pair) {

    //  Store item on the display (symbol code)
    move(y, x);                         // Move cursor to (y,x)
    attron(COLOR_PAIR(color_pair));     // Start writing in selected color
    addch(symbol);                      // Store symbol on the virtual display
    attroff(COLOR_PAIR(color_pair));    // Stop writing in selected color

    // Store the type of item that was placed
    aboard->cells[y][x] = board_code;
}

enum ResCodes initializeBoard(struct board* aboard) {
    int y;
    // Maximal index of a row, reserve space for message area
    aboard->last_row = LINES - ROWS_RESERVED - 1;
    // Maximal index of a column
    aboard->last_col = COLS - 1;

    // Check dimensions of the board
    if ( COLS < MIN_NUMBER_OF_COLS ||
          LINES < MIN_NUMBER_OF_ROWS + ROWS_RESERVED) {
        char buf[100];
        sprintf(buf, "Das Fenster ist zu klein: wir brauche %dx%d",
                MIN_NUMBER_OF_COLS, MIN_NUMBER_OF_ROWS + ROWS_RESERVED );
        showDialog(buf, "Bitte eine Taste druecken");
        return RES_FAILED;
    }
    // Allocate memory for 2-dimensional array of cells
    // Alloc aray of rows
    aboard->cells = malloc(sizeof(int*) * LINES);
    if (aboard->cells == NULL) {
      showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste druecken");
      exit(RES_FAILED); // No memory -> direct exit
    }
    for (y = 0; y < LINES; y++) {
      // Allocate array of columns for each y
      aboard->cells[y] = malloc(sizeof(int) * COLS);
      if (aboard->cells [y] == NULL) {
        showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste druecken");
        exit(RES_FAILED); // No memory -> direct exit
      }
    }
    return RES_OK;
}

enum ResCodes initializeLevel(struct board* aboard) {
    int y,x;
    // Fill board and screen buffer with empty cells.
    for (y = 0; y <= aboard->last_row ; y++) {
        for(x = 0; x <= aboard->last_col ; x++) {
            placeItem(aboard,y,x,BC_FREE_CELL,SYMBOL_FREE_CELL,COLP_FREE_CELL);
        }
    }
    // Draw a line in order to seperate the message area
    // Note: we cannot use function placeItem() since the message area
    // is outside the board!
    y = aboard->last_row + 1;
    for (x=0; x <= aboard->last_col ; x++) {
        move(y, x),
        attron(COLOR_PAIR(COLP_BARRIER));
        addch(SYMBOL_BARRIER);
        attroff(COLOR_PAIR(COLP_BARRIER));
    }
    //Barriers: use a loop
    for (y = 5; y <= 16; y++) {
        x = 15;
        placeItem(aboard,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
    }
    for (y=aboard->last_row - 1; y >= aboard->last_row - 14 ; y-- ) {
        x = 60;
        placeItem(aboard,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
    }
    // Food
    // Food 1
    placeItem(aboard, 3, 3, BC_FOOD_1,SYMBOL_FOOD_1,COLP_FOOD_1);
    placeItem(aboard, 3, aboard->last_col-10, BC_FOOD_1,SYMBOL_FOOD_1,COLP_FOOD_1);
    // Food 2
    placeItem(aboard, 20, 7, BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
    placeItem(aboard, 10, 37, BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
    placeItem(aboard, 23, 34, BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
    placeItem(aboard, 8, 9, BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
    // Food 3
    placeItem(aboard, 22, 30, BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
    placeItem(aboard, 13, 42, BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
    placeItem(aboard, 0, 35, BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);   
    placeItem(aboard, aboard->last_row-1, aboard->last_col-1, BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);

    // Initialize number of food items
    aboard->food_items = 10;
    return RES_OK;
}

void cleanupBoard(struct board* aboard) {
  int y;
  for (y = 0; y < LINES; y++) {
    free(aboard->cells[y]);
  }
  free(aboard->cells);
}

// Getters

// Get the last usable row on the display
int getLastRowOnBoard(struct board* aboard) {
    return aboard->last_row;
}

// Get the last usable column on the display
int getLastColOnBoard(struct board* aboard) {
    return aboard->last_col;
}

// Get the number of food items left on the board
int getNumberOfFoodItems(struct board* aboard) {
    return aboard->food_items;
}

// Get the item code of a position's item
enum BoardCodes getContentAt(struct board* aboard, struct pos position) {
    return aboard->cells[position.y][position.x];
}

// Setter

// Set the number of food items on the board
void setNumberOfFoodItems(struct board* aboard, int n) {
    aboard->food_items = n;
}

// Set the number of food items to a decremented number
void decrementNumberOfFoodItems(struct board* aboard) {
    aboard->food_items = aboard->food_items - 1;
}
