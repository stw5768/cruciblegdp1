#include <stdio.h>

#define name "Karl"
#define day 01
#define month 01
#define year 1999

int main (void)
{
  printf("Mein Name ist %s\n", name);
  printf("Mein Geburtsdatum ist der %02d. %02d. %04d\n", day,month,year);
  return 0;
}
